import express from "express";
import createError from "http-errors";
import cookieParser from "cookie-parser";
import logger from "morgan";
import mongoose from "mongoose";
import path from "path";
import fileUpload from "express-fileupload";
import {resErr} from "./components/helpers/response";
import apiRouter from "./routes/api";
import utilityRouter from "./routes/utility";

const app = express();

// Db
mongoose.connect(process.env.MONGODB_CONNECT, {
    useCreateIndex: true,
    useNewUrlParser: true
});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('we are connected!');
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(fileUpload());

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));
app.use('/files', express.static(path.join(__dirname, 'files')));

// API Routes
app.use('/api', apiRouter);
app.use('/utility', utilityRouter);

// Handles any requests that don't match the ones above
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/client/build/index.html'));
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res) {
    res.status(err.status || 500);
    res.json(resErr({
        message: err.message,
        status: err.status,
        stack: req.app.get('env') === 'development' ? err : {}
    }));
});

module.exports = app;
