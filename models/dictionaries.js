import mongoose, {Schema} from "mongoose";

const dictionarySchema = new Schema({
    type: {
        type: String,
        enum: ['status', 'priority', 'title'],
        default: 'statuses'
    },
    name: {
        type: String,
        trim: true
    },
    value: {
        type: String,
        trim: true
    },
    status_type: {
        type: String,
        enum: [null, 'is_lost', 'is_won', 'is_bill_introduced']
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('dictionary', dictionarySchema, 'dictionary');
