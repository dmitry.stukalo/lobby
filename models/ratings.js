import mongoose, {Schema} from "mongoose";

const ratingSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    rating: {
        type: Number,
        min: 1,
        max: 5
    },
    user_from: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('rating', ratingSchema);
