import mongoose, {Schema} from "mongoose";

const stateSchema = new Schema({
    state_id: {
        type: Schema.Types.ObjectId,
        ref: 'state'
    },
    year: {
        type: String,
        min: 2019,
        max: 2030,
        default: 2019
    },
    session_ends: Date,
    lobbyist_hired: Date,
    hb_introduced: Date,
    hb_introduced_file: String,
    sb_introduced: Date,
    sb_introduced_file: String,
    passed_house_committee: Date,
    passed_senate_committee: Date,
    passed_house: Date,
    passed_senate: Date,
    became_law: Date
}, {
    timestamps: true
});

module.exports = mongoose.model('state_date', stateSchema, 'state_date');
