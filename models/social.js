import mongoose, {Schema} from "mongoose";

const socialSchema = new Schema({
    provider: String,
    id: String,
    name: String,
    firstName: String,
    lastName: String,
    email: String,
    profilePicURL: String,
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("social", socialSchema);
