import mongoose, {Schema} from "mongoose";
import searchable from "mongoose-regex-search";

const stateSchema = new Schema({
    letters: {
        type: String,
        uppercase: true,
        trim: true,
        unique: true
    },
    name: {
        type: String,
        trim: true,
        unique: true,
        searchable: true
    },
    capital: {
        type: String,
        searchable: true
    },
    status: {
        type: Schema.Types.ObjectId,
        ref: 'dictionary'
    },
    priority: {
        type: Number,
        min: 0,
        max: 10,
        default: 0
    },
    description: {
        type: String,
        searchable: true
    },
    overbillings: [{
        type: Schema.Types.ObjectId,
        ref: 'overbilling'
    }],
    targets: [{
        type: Schema.Types.ObjectId,
        ref: 'target'
    }],
    lobbyists: [{
        type: Schema.Types.ObjectId,
        ref: 'user'
    }],
    dates: [{
        type: Schema.Types.ObjectId,
        ref: 'state_date'
    }],
}, {
    timestamps: true
});

stateSchema.plugin(searchable);

module.exports = mongoose.model('state', stateSchema);
