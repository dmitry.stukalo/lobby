import mongoose, {Schema} from "mongoose";

const targetSchema = new Schema({
    title: {
        type: Schema.Types.ObjectId,
        ref: 'dictionary'
    },
    name: {
        type: String,
        trim: true
    },
    priority: {
        type: Number,
        min: 1,
        max: 5,
        default: 5
    },
    party: {
        type: String,
        enum: ['r', 'd'],
        default: 'r'
    },
    status: {
        type: String,
        enum: ['supporter', 'targeting', 'not-targeting', 'declined'],
        default: 'not-targeting'
    },
    info: {
        type: String,
        trim: true
    },
    stateId: {
        type: Schema.Types.ObjectId,
        ref: 'state'
    },
    lobbyists: [{
        type: Schema.Types.ObjectId,
        ref: 'user'
    }]
}, {
    timestamps: true
});

module.exports = mongoose.model('target', targetSchema);
