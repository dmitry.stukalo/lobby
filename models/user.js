import mongoose, {Schema} from "mongoose";
import crypto from "crypto";
import jwt from "jsonwebtoken";
import uniqueValidator from "mongoose-unique-validator";
import searchable from "mongoose-regex-search";

const userSchema = new Schema({
    name: {
        type: String,
        searchable: true
    },
    email: {
        type: String,
        trim: true,
        unique: true,
        searchable: true
    },
    hashed_password: String,
    salt: String,
    role: {
        type: String,
        enum: ['superadmin', 'admin', 'lobbyist', 'reviewer']
    },
    invitation: String,
    photo: String,
    phone: String,
    skype: String,
    linkedin: String,
    location: String,
    address: String,
    started: String,
    other_states: String,
    compensation: Number,
    compensation_file: String,
    stock: Number,
    stock_date: Date,
    additional_info: String,
    change_password_token: String,
    last_login: {
        type: Date,
        default: new Date()
    },
    company_id: {
        type: Schema.Types.ObjectId,
        ref: 'company'
    },
    social_connect: [{
        type: Schema.Types.ObjectId,
        ref: 'social'
    }],
    states: [{
        type: Schema.Types.ObjectId,
        ref: 'state'
    }],
    notes: [{
        type: Schema.Types.ObjectId,
        ref: 'note'
    }],
    rating: [{
        type: Schema.Types.ObjectId,
        ref: 'rating'
    }],
    payments: [{
        type: Schema.Types.ObjectId,
        ref: 'payment'
    }]
}, {
    timestamps: true
});

// Virtual field for encrypt password
userSchema.virtual('encrypt_password').set(function (password) {
    this.salt = this.makeSalt();
    this.hashed_password = this.encryptPassword(password);
});

// Methods
userSchema.methods = {
    encryptPassword: function (password) {
        return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
    },
    authenticate: function (plainText) {
        return this.encryptPassword(plainText) === this.hashed_password;
    },
    makeSalt: function () {
        return Math.round((new Date().valueOf() * Math.random())) + '';
    },
    getToken: function () {
        return jwt.sign({
            id: this._id.toString(),
            role: this.role
        }, process.env.JWT_SECRET, {expiresIn: process.env.JWT_SECRET_EXPIRES_IN || 86400});
    }
};

// Get user by email
userSchema.query.byEmail = function (email) {
    return this.where({email: new RegExp(email, 'i')});
};

// Apply the uniqueValidator and searchable plugin to userSchema.
userSchema.plugin(uniqueValidator);
userSchema.plugin(searchable);

module.exports = mongoose.model("user", userSchema);
