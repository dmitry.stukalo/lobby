import mongoose, {Schema} from "mongoose";

const paymentSchema = new Schema({
    amount: {
        type: Number
    },
    date: {
        type: Date
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('payment', paymentSchema, 'payment');
