import mongoose, {Schema} from "mongoose";
import searchable from "mongoose-regex-search";

const companySchema = new Schema({
    name: {
        type: String,
        searchable: true
    },
    url: String,
    users: [{
        type: Schema.Types.ObjectId,
        ref: 'user'
    }]
}, {
    timestamps: true
});

// Apply the eachelle plugin to searchable.
companySchema.plugin(searchable);

module.exports = mongoose.model('company', companySchema, 'company');
