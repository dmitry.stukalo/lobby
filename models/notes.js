import mongoose, {Schema} from "mongoose";

const noteSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    state_id: {
        type: Schema.Types.ObjectId,
        ref: 'state'
    },
    parent_note_id: {
        type: String,
        default: null
    },
    message: {
        type: String,
        trim: true
    },
    priority: {
        type: Schema.Types.ObjectId,
        ref: 'dictionary'
    },
    color: {
        type: String,
        enum: ['gray', 'green', 'light-green', 'blue', 'pink'],
        default: 'gray'
    },
    is_active: {
        type: Boolean
    },
    notes: [{
        type: Schema.Types.ObjectId,
        ref: 'note'
    }]
}, {
    timestamps: true
});

// Indexes
noteSchema.index({
    message: 'text'
});

module.exports = mongoose.model("note", noteSchema);
