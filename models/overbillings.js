import mongoose, {Schema} from "mongoose";

const overbillingSchema = new Schema({
    state_id: {
        type: Schema.Types.ObjectId,
        ref: 'state'
    },
    company: {
        type: String
    },
    amount: Number,
    year: {
        type: Number,
        min: 1990,
        max: 2030,
    },
    category: {
        type: String
    },
    preventable_by_tb: {
        type: Boolean,
        default: false
    },
    gist: {
        type: String
    },
    url: String,
    details: {
        type: String
    }
}, {
    timestamps: true
});

// Indexes
overbillingSchema.index({
    company: 'text',
    category: 'text',
    gist: 'text',
    details: 'text'
}, {
    name: 'category_gist_details',
    weights: {
        company: 1,
        category: 2,
        gist: 3,
        details: 4
    }
});

module.exports = mongoose.model('overbilling', overbillingSchema, 'overbilling');