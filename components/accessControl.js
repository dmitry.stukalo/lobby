const rules = [
    // Reg & auth
    {path: 'post /invitation', roles: ['superadmin', 'admin']},
    {path: 'post /check_token', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},

    // Social auth & registration & connect
    {path: 'post /social/connect', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'delete /social/connect/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},

    // States
    {path: 'get /states', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'get /states-about/:letters', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'get /states/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'put /states/:id', roles: ['superadmin', 'admin', 'lobbyist']},
    {path: 'put /states-dates/:id/:year', roles: ['superadmin', 'admin', 'lobbyist']},

    // Profile
    {path: 'get /profile/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'put /profile/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'put /profile', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},

    // Target
    {path: 'get /targets/:letters', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'get /target/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'post /target', roles: ['superadmin', 'admin', 'lobbyist']},
    {path: 'put /target/:id', roles: ['superadmin', 'admin', 'lobbyist']},
    {path: 'delete /target/:id', roles: ['superadmin', 'admin']},
    {path: 'put /target/lobbyist/:id', roles: ['superadmin', 'admin']},
    {path: 'delete /target/lobbyist/:id', roles: ['superadmin', 'admin']},

    // Lobbyist
    {path: 'get /lobbyists', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'post /lobbyist', roles: ['superadmin', 'admin']},
    {path: 'post /lobbyist/relation', roles: ['superadmin', 'admin']},
    {path: 'get /lobbyists', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'get /lobbyist/states/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'get /lobbyist/users/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'delete /lobbyist/relation', roles: ['superadmin', 'admin']},
    {path: 'delete /lobbyist/:id', roles: ['superadmin', 'admin']},

    // Notes
    {path: 'get /notes/:parent/:state/:limit', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'get /note/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'get /notes-state/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'post /note', roles: ['superadmin', 'admin', 'lobbyist']},
    {path: 'put /note/:id', roles: ['superadmin', 'admin', 'lobbyist']},
    {path: 'delete /note/:id', roles: ['superadmin', 'admin']},

    // Ratings
    {path: 'get /rating/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'post /rating', roles: ['superadmin', 'admin']},

    // Payments
    {path: 'post /payment', roles: ['superadmin', 'admin']},
    {path: 'get /payment/:id', roles: ['superadmin', 'admin']},
    {path: 'put /payment/:id', roles: ['superadmin', 'admin']},
    {path: 'delete /payment/:id', roles: ['superadmin', 'admin']},

    // Upload file
    {path: 'post /upload-file/:type', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},

    // Admin
    {path: 'get /admins', roles: ['superadmin']},
    {path: 'delete /admin/:id', roles: ['superadmin']},

    // Dictionaries
    {path: 'get /dictionaries', roles: ['superadmin', 'admin', 'lobbyist']},
    {path: 'get /dictionary/:id', roles: ['superadmin', 'admin', 'lobbyist']},
    {path: 'post /dictionary', roles: ['superadmin', 'admin']},
    {path: 'put /dictionary/:id', roles: ['superadmin', 'admin']},
    {path: 'delete /dictionary/:id', roles: ['superadmin', 'admin']},

    // Companies
    {path: 'get /companies', roles: ['superadmin', 'admin', 'lobbyist', 'lobbyist', 'reviewer']},
    {path: 'get /company/:id', roles: ['superadmin', 'admin']},
    {path: 'post /company', roles: ['superadmin', 'admin', 'lobbyist', 'lobbyist', 'reviewer']},
    {path: 'put /company/:id', roles: ['superadmin', 'admin']},
    {path: 'delete /company/:id', roles: ['superadmin', 'admin']},

    // Overbilling
    {path: 'get /overbillings', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'get /overbilling/:id', roles: ['superadmin', 'admin', 'lobbyist', 'reviewer']},
    {path: 'post /overbilling', roles: ['superadmin', 'admin']},
    {path: 'put /overbilling/:id', roles: ['superadmin', 'admin']},
    {path: 'delete /overbilling/:id', roles: ['superadmin', 'admin']},

    // Search
    {path: 'post /search', roles: ['superadmin', 'admin', 'lobbyist', 'lobbyist', 'reviewer']},

    // Utility
    {path: 'get /fill-map', roles: ['superadmin']},
    {path: 'get /drop-indexes', roles: ['superadmin']}
];

function accessControl(req) {
    const method_path = Object.entries(req.route.methods).map(([key]) => {
        return key;
    })[0] + ' ' + req.route.path;

    const rule = rules.find(v => v.path === method_path);
    if (rule) {
        const role = rule.roles.find(v => v === req.userRole);
        return role === req.userRole;
    }

    return false;
}

module.exports = {accessControl};
