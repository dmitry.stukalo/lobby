import States from '../../models/states';
import Dictionaries from '../../models/dictionaries';
import {resOk, resErr} from "../helpers/response";

function fillMap(req, res) {

    const statesArray = [{
        letters: 'ak',
        name: 'Alaska',
        capital: 'Juneau'
    }, {
        letters: 'al',
        name: 'Alabama',
        capital: 'Montgomery'
    }, {
        letters: 'ar',
        name: 'Arkansas',
        capital: 'Little Rock'
    }, {
        letters: 'az',
        name: 'Arizona',
        capital: 'Phoenix'
    }, {
        letters: 'ca',
        name: 'California',
        capital: 'Sacramento'
    }, {
        letters: 'co',
        name: 'Colorado',
        capital: 'Denver'
    }, {
        letters: 'ct',
        name: 'Connecticut',
        capital: 'Hartford'
    }, {
        letters: 'de',
        name: 'Delaware',
        capital: 'Dover'
    }, {
        letters: 'fl',
        name: 'Florida',
        capital: 'Tallahassee'
    }, {
        letters: 'ga',
        name: 'Georgia',
        capital: 'Atlanta'
    }, {
        letters: 'hi',
        name: 'Hawaii',
        capital: 'Honolulu'
    }, {
        letters: 'ia',
        name: 'Iowa',
        capital: 'Des Moines'
    }, {
        letters: 'id',
        name: 'Idaho',
        capital: 'Boise'
    }, {
        letters: 'il',
        name: 'Illinois',
        capital: 'Springfield'
    }, {
        letters: 'in',
        name: 'Indiana',
        capital: 'Indianapolis'
    }, {
        letters: 'ks',
        name: 'Kansas',
        capital: 'Topeka'
    }, {
        letters: 'ky',
        name: 'Kentucky',
        capital: 'Frankfort'
    }, {
        letters: 'la',
        name: 'Louisiana',
        capital: 'Baton Rouge'
    }, {
        letters: 'ma',
        name: 'Massachusetts',
        capital: 'Boston'
    }, {
        letters: 'md',
        name: 'Maryland',
        capital: 'Annapolis'
    }, {
        letters: 'me',
        name: 'Maine',
        capital: 'Augusta'
    }, {
        letters: 'mi',
        name: 'Michigan',
        capital: 'Lansing'
    }, {
        letters: 'mn',
        name: 'Minnesota',
        capital: 'St. Paul'
    }, {
        letters: 'mo',
        name: 'Missouri',
        capital: 'Jefferson City'
    }, {
        letters: 'ms',
        name: 'Mississippi',
        capital: 'Jackson'
    }, {
        letters: 'mt',
        name: 'Montana',
        capital: 'Helena'
    }, {
        letters: 'nc',
        name: 'North Carolina',
        capital: 'Raleigh'
    }, {
        letters: 'nd',
        name: 'North Dakota',
        capital: 'Bismarck'
    }, {
        letters: 'ne',
        name: 'Nebraska',
        capital: 'Lincoln'
    }, {
        letters: 'nh',
        name: 'New Hampshire',
        capital: 'Concord'
    }, {
        letters: 'nj',
        name: 'New Jersey',
        capital: 'Trenton'
    }, {
        letters: 'nm',
        name: 'New Mexico',
        capital: 'Santa Fe'
    }, {
        letters: 'nv',
        name: 'Nevada',
        capital: 'Carson City'
    }, {
        letters: 'ny',
        name: 'New York',
        capital: 'Albany'
    }, {
        letters: 'oh',
        name: 'Ohio',
        capital: 'Columbus'
    }, {
        letters: 'ok',
        name: 'Oklahoma',
        capital: 'Oklahoma City'
    }, {
        letters: 'or',
        name: 'Oregon',
        capital: 'Salem'
    }, {
        letters: 'pa',
        name: 'Pennsylvania',
        capital: 'Harrisburg'
    }, {
        letters: 'ri',
        name: 'Rhode Island',
        capital: 'Providence'
    }, {
        letters: 'sc',
        name: 'South Carolina',
        capital: 'Columbia'
    }, {
        letters: 'sd',
        name: 'South Dakota',
        capital: 'Pierre'
    }, {
        letters: 'tn',
        name: 'Tennessee',
        capital: 'Nashville'
    }, {
        letters: 'tx',
        name: 'Texas',
        capital: 'Austin'
    }, {
        letters: 'ut',
        name: 'Utah',
        capital: 'Salt Lake City'
    }, {
        letters: 'va',
        name: 'Virginia',
        capital: 'Richmond'
    }, {
        letters: 'vt',
        name: 'Vermont',
        capital: 'Montpelier'
    }, {
        letters: 'wa',
        name: 'Washington',
        capital: 'Olympia'
    }, {
        letters: 'wi',
        name: 'Wisconsin',
        capital: 'Madison'
    }, {
        letters: 'wv',
        name: 'West Virginia',
        capital: 'Charleston'
    }, {
        letters: 'wy',
        name: 'Wyoming',
        capital: 'Cheyenne'
    }];

    Dictionaries.findOne({type: 'status'}, function (err, dictionary) {
        const states = statesArray.map(v => {
            return {
                letters: v.letters.toUpperCase(),
                name: v.name,
                capital: v.capital,
                status: dictionary._id,
                priority: 0
            };
        });

        States.collection.drop(function () {
            States.collection.insertMany(states, function (err) {
                if (err) return res.json(resErr(err));
                res.json(resOk('ok'));
            });
        });
    });

}

module.exports = {fillMap};