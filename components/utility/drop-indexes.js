import User from '../../models/user';
import States from '../../models/states';
import Targets from '../../models/targets';
import Socials from '../../models/social';
import {resOk, resErr} from "../helpers/response";

function dropIndexes(req, res) {
    User.collection.dropIndexes(err => {
        if (err) return res.json(resErr(err));
        States.collection.dropIndexes(err => {
            if (err) return res.json(resErr(err));
            Targets.collection.dropIndexes(() => {
                Socials.collection.dropIndexes(err => {
                    if (err) return res.json(resErr(err));
                    res.json(resOk('ok'));
                });
            });
        });
    });
}

module.exports = {dropIndexes};