import jwt from "jsonwebtoken";
import {resErr} from "./helpers/response";
import {accessControl} from "./accessControl";

function middleware(req, res, next) {
    const token = (req.headers['authorization'] || '').split(' ')[1];
    if (!token) return res.json(resErr('No token provided.'));
    jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
        if (err) return res.json(resErr('Failed to authenticate token'));
        req.userId = decoded.id;
        req.userRole = decoded.role;
        if (accessControl(req)) {
            next();
        } else {
            return res.json(resErr('Sorry, access denied, please check your permissions'));
        }
    });
}

module.exports = middleware;
