import Email from "email-templates";
import nodemailer from "nodemailer";

function sendMail(template, email_to, variables, send_result) {
    const transport = nodemailer.createTransport({
        host: process.env.SMTP_HOST,
        port: process.env.SMTP_PORT,
        secureConnection: process.env.SMTP_TLS,
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASS
        },
        tls: {
            ciphers: 'SSLv3'
        }
    });

    const sendMail = new Email({
        message: {
            from: process.env.EMAIL_FROM + ' <' + process.env.EMAIL_FROM + '>',
            to: email_to
        },
        send: true,
        transport: transport,
        views: {
            options: {
                extension: 'ejs'
            }
        }
    });

    sendMail.send({
        template: template,
        locals: variables
    }).then(res => {
        send_result && send_result({
            status: true,
            result: res
        });
    }).catch(err => {
        send_result && send_result({
            status: false,
            error: err
        });
    });
}

module.exports = {sendMail};
