const resOk = result => {
    return {
        status: true,
        error: null,
        result: result
    };
};

const resErr = error => {
    let message = error;
    if (typeof error === 'string') {
        message = {
            message: error
        };
    } else {
        if (typeof error !== 'undefined') {
            // For MongoDB
            if ('errmsg' in error) {
                message = {
                    message: error.errmsg
                };
            }
            // For Send mail
            if ('response' in error) {
                message = {
                    message: error.response
                };
            }
        } else {
            message = {
                message: 'error'
            };
        }
    }

    return {
        status: false,
        error: message,
        result: null
    };
};

module.exports = {resOk, resErr};
