import fs from "fs";

function upload_file(type, newFile, oldFile) {
    clearTempDir();
    if (newFile && newFile !== oldFile) {
        if (oldFile && fs.existsSync(__dirname + '/../../files/' + type + '/' + oldFile)) {
            fs.unlinkSync(__dirname + '/../../files/' + type + '/' + oldFile);
        }
        if (fs.existsSync(__dirname + '/../../files/temp/' + newFile)) {
            fs.renameSync(__dirname + '/../../files/temp/' + newFile, __dirname + '/../../files/' + type + '/' + newFile);
        }
        return newFile;
    } else {
        return oldFile;
    }
}

function clearTempDir() {
    const lifeTime = 1000 * 60 * 60 * 24; // 24 hours
    const path = __dirname + '/../../files/temp';
    fs.readdir(path, (err, items) => {
        items.forEach(fileName => {
            if (fileName !== '.gitignore') {
                const file = path + '/' + fileName;
                fs.stat(file, function () {
                    return function (err, stats) {
                        if (stats && (new Date()).getTime() - stats.birthtimeMs > lifeTime && fs.existsSync(file)) {
                            fs.unlinkSync(file);
                        }
                    }
                }(file));
            }
        });
    });
}

module.exports = {upload_file};
