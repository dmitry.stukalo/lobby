import Dictionary from "../../models/dictionaries";
import States from "../../models/states";
import Notes from "../../models/notes";
import Targets from "../../models/targets";
import {resOk, resErr} from "../helpers/response";

function getDictionaries(req, res) {
    Dictionary.find()
        .populate('states notes targets')
        .exec(function (err, dictionaries) {
                if (err) return res.json(resErr(err));
                res.json(resOk(dictionaries));
            }
        );
}

function getDictionary(req, res) {
    const {id} = req.params;

    Dictionary.findById(id)
        .populate('states notes targets')
        .exec(function (err, dictionary) {
                if (err) return res.json(resErr(err));
                if (dictionary) {
                    res.json(resOk(dictionary));
                } else {
                    return res.json(resErr("Dictionary item not found"));
                }
            }
        );
}

function createDictionary(req, res) {
    const {type, name, value, status_type} = req.body;

    if (!type) {
        return res.json(resErr("Type is required!"));
    }

    if (!name) {
        return res.json(resErr("Name is required!"));
    }

    if (!value) {
        return res.json(resErr("Please enter value!"));
    }

    Dictionary.findOne({status_type}, function(err, dictionaries){
        if (err) return res.json(resErr(err));
        if (status_type !== null &&  dictionaries) return res.json(resErr('This type of status is already exist'));
        Dictionary.create({
            type, name, value, status_type
        },
            function (err) {
                if (err) return res.json(resErr(err));
                res.json(resOk('ok'));
            }
        );
    })
}

function updateDictionary(req, res) {
    const {id} = req.params;
    const {name, value, status_type} = req.body;

    if (!id) {
        return res.json(resErr("Dictionary item ID is required!"));
    }

    if (!name) {
        return res.json(resErr("Name is required!"));
    }

    if (!value) {
        return res.json(resErr("Please enter value!"));
    }

    Dictionary.findOne({status_type}, function(err, dictionaries){
        if (err) return res.json(resErr(err));
        if (status_type !== null && dictionaries) return res.json(resErr('This type of status is already exist'));
        Dictionary.findByIdAndUpdate(id,
            {name, value, status_type},
            function (err, dictionary) {
                if (err) return res.json(resErr(err));
                if (!dictionary) return res.json(resErr('Dictionary item not found'));
                res.json(resOk('ok'));
            });
    });
}

function deleteDictionary(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("Dictionary item ID is required!"));
    }

    Dictionary.findById(id, function (err, dictionary) {
        if (err) return res.json(resErr(err));
        if (dictionary) {
            if (dictionary.type === 'status') {
                States.findOne({status: dictionary._id}, function (err, state) {
                    if (err) return res.json(resErr(err));
                    if (state) return res.json(resErr('Status already used in this State'));
                    dictionary.remove(function (err) {
                        if (err) return res.json(resErr(err));
                        res.json(resOk('ok'));
                    });
                });
            }
            if (dictionary.type === 'priority') {
                Notes.findOne({priority: dictionary._id}, function (err, note) {
                    if (err) return res.json(resErr(err));
                    if (note) return res.json(resErr('Priority already used in this Note'));
                    dictionary.remove(function (err) {
                        if (err) return res.json(resErr(err));
                        res.json(resOk('ok'));
                    });
                });
            }
            if (dictionary.type === 'title') {
                Targets.findOne({title: dictionary._id}, function (err, target) {
                    if (err) return res.json(resErr(err));
                    if (target) return res.json(resErr('Title already used for this Official'));
                    dictionary.remove(function (err) {
                        if (err) return res.json(resErr(err));
                        res.json(resOk('ok'));
                    });
                });
            }
        } else {
            return res.json(resErr('Dictionary item not found'));
        }
    });
}

module.exports = {getDictionaries, getDictionary, createDictionary, updateDictionary, deleteDictionary};
