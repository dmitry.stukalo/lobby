import User from "../../models/user";
import {resOk, resErr} from "../helpers/response";
import {sendMail} from "../helpers/send_mail";
import EmailValidator from "email-validator";
import Crypto from "crypto";

function registration(req, res) {
    const {invitation, name, password, password_repeat} = req.body;

    if (!invitation) {
        return res.json(resErr("Invitation is required"));
    }

    if (!name) {
        return res.json(resErr("Name is required"));
    }

    if (!password) {
        return res.json(resErr("Password is required"));
    }

    if (!password_repeat) {
        return res.json(resErr("Password confirmation is required"));
    }

    if (password !== password_repeat) {
        return res.json(resErr("Passwords do not match, please check"));
    }

    User.findOne({invitation}, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            user.name = name;
            user.encrypt_password = password;
            user.invitation = null;
            user.save(function (err) {
                if (err) return res.json(resErr(err));
                res.json(resOk({
                    token: user.getToken()
                }));
            });
        } else {
            res.json(resErr('User profile not found doesn\'t exist, please check'));
        }
    });
}

function authenticate(req, res) {
    const {email, password} = req.body;

    if (!email) {
        return res.json(resErr("Email can not be empty"));
    }

    if (!EmailValidator.validate(email)) {
        return res.json(resErr("Email format is not valid, please check"));
    }

    if (!password) {
        return res.json(resErr("Password is required"));
    }

    User.findOne().byEmail(email).exec(function (err, user) {
        if (user && user.authenticate(password)) {
            res.json(resOk({
                token: user.getToken()
            }));
        } else {
            res.json(resErr("Login or password incorrect, please try again"));
        }
    });
}

function check_token(req, res) {
    User.findByIdAndUpdate(req.userId,
        {last_login: new Date()},
        function (err, user) {
            if (err) return res.json(resErr(err));
            if (user) {
                res.json(resOk({
                    id: user._id.toString(),
                    role: user.role,
                    email: user.email,
                    notes: user.notes,
                    states: user.states
                }));
            } else {
                res.json(resErr('User profile not found doesn\'t exist, please check'));
            }
        }
    );
}

function check_invitation(req, res) {
    const {code} = req.body;

    if (!code) {
        return res.json(resErr("Invitation code expired or incorrect"));
    }

    User.findOne({
        invitation: code
    }, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            res.json(resOk({
                id: user._id.toString(),
                email: user.email
            }));
        } else {
            res.json(resErr('Invitation code not be found or doesn\'t exist, please check'));
        }
    });
}

function invitation(req, res) {
    const {email, role} = req.body;

    if (!email) {
        return res.json(resErr("Email can not be empty"));
    }

    if (!EmailValidator.validate(email)) {
        return res.json(resErr("Email format is not valid, please check"));
    }

    if (!role) {
        return res.json(resErr("Role is required"));
    }

    if (role === 'admin' && req.userRole !== 'superadmin') {
        return res.json(resErr('Sorry, access denied, please check your permissions'));
    }

    const token = Crypto.randomBytes(48).toString('hex');

    User.findOne().byEmail(email).exec(function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            res.json(resErr('Sorry, this email already exists, please try another email'));
        } else {
            User.create({
                    email: email,
                    role: role,
                    invitation: token,
                    salt: null,
                    hashed_password: null
                },
                function (err) {
                    if (err) return res.json(resErr(err));
                    User.findById(req.userId, function (err, invaded_user) {
                        if (err) return res.json(resErr(err));
                        sendMail('invitation', email, {
                            host: process.env.HOST,
                            invitation: token,
                            name: invaded_user.name,
                            emailFrom: process.env.EMAIL_FROM
                        }, sendres => {
                            if (sendres.status) {
                                res.json(resOk({
                                    invitation_link: 'https://' + process.env.HOST + '/registration/' + token,
                                    invitation_code: token
                                }));
                            } else {
                                return res.json(resErr(sendres.error));
                            }
                        });
                    });
                }
            );
        }
    });
}

function restorePassword(req, res) {
    const {email} = req.body;

    if (!email) {
        return res.json(resErr("Email can not be empty"));
    }

    User.findOne().byEmail(email).exec(function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            const token = Crypto.randomBytes(48).toString('hex');
            user.change_password_token = token;
            user.save(function (err) {
                if (err) return res.json(resErr(err));
                sendMail('restore_password', email, {
                    host: process.env.HOST,
                    token: token,
                    name: user.name,
                    emailFrom: process.env.EMAIL_FROM
                }, sendres => {
                    if (sendres.status) {
                        res.json(resOk('ok'));
                    } else {
                        return res.json(resErr(sendres.error));
                    }
                });
            });
        } else {
            return res.json(resErr('Email can not be found or doesn\'t exist, please check'));
        }
    });
}

function changePassword(req, res) {
    const {
        token, new_password, new_password_confirm
    } = req.body;

    if (!token) {
        return res.json(resErr("Token is required"));
    }

    if (!new_password) {
        return res.json(resErr("New password is required"));
    }

    if (!new_password_confirm) {
        return res.json(resErr("Password confirmation can not be empty"));
    }

    if (new_password !== new_password_confirm) {
        return res.json(resErr('Passwords do not match, please check'));
    }

    User.findOne({change_password_token: token}, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            user.encrypt_password = new_password;
            user.change_password_token = null;
            user.save(function (err) {
                if (err) return res.json(resErr(err));
                sendMail('password', user.email, {
                    host: process.env.HOST,
                    password: new_password,
                    emailFrom: process.env.EMAIL_FROM
                });
                res.json(resOk({
                    token: user.getToken()
                }));
            });
        } else {
            res.json(resErr("User profile not found doesn\'t exist, please check"));
        }
    });
}

module.exports = {
    registration,
    authenticate,
    check_token,
    invitation,
    check_invitation,
    restorePassword,
    changePassword
};
