import User from "../../models/user";
import Social from "../../models/social";
import {resOk, resErr} from "../helpers/response";
import Crypto from "crypto";
import fs from "fs";
import https from "https";
import {sendMail} from "../helpers/send_mail";
import path from "path";

function auth(req, res) {
    const {_provider, _profile, _token} = req.body;

    if (!_provider || !_profile || !_token) {
        return res.json(resErr('Unable to receive data from provider!'));
    }

    Social.findOne({
        provider: _provider,
        id: _profile.id
    }, function (err, social) {
        if (err) return res.json(resErr(err));
        if (social) {
            User.findById(social.userId, function (err, user) {
                if (err) return res.json(resErr(err));
                if (user) {
                    res.json(resOk({
                        token: user.getToken()
                    }));
                } else {
                    res.json(resErr('User profile not found doesn\'t exist, please check'));
                }
            });
        } else {
            if (_profile.email) {
                User.findOne().byEmail(_profile.email).exec(function (err, user) {
                    if (err) return res.json(resErr(err));
                    if (user) {
                        Social.create({
                            provider: _provider,
                            id: _profile.id,
                            name: _profile.name,
                            firstName: _profile.firstName,
                            lastName: _profile.lastName,
                            email: _profile.email,
                            profilePicURL: _profile.profilePicURL,
                            userId: user._id
                        }, function (err, social) {
                            if (err) return res.json(resErr(err));
                            User.findByIdAndUpdate(user._id, {
                                    $addToSet: {social_connect: social._id}
                                }, {safe: true, upsert: true},
                                function (err) {
                                    if (err) return res.json(resErr(err));
                                    res.json(resOk({
                                        token: user.getToken()
                                    }));
                                }
                            );
                        });
                    } else {
                        res.json(resErr('User profile not found doesn\'t exist, please check'));
                    }
                });
            } else {
                res.json(resErr('User profile not found doesn\'t exist, please check'));
            }
        }
    });
}

function downloadProfilePhoto(url) {
    const fileExtension = path.extname(url).toLowerCase();
    const fileName = Crypto.randomBytes(16).toString('hex') + fileExtension;
    const file = fs.createWriteStream(__dirname + '/../../files/profile/' + fileName);
    https.get(url, res => {
        res.pipe(file);
    });
    return fileName;
}

function registration(req, res) {
    const {invitation, social} = req.body;

    if (!invitation) {
        return res.json(resErr("Invitation is required"));
    }
    if (!social._provider) {
        return res.json(resErr("Social profile is required"));
    }

    User.findOne({invitation}, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            Social.findOne({
                provider: social._provider,
                id: social._profile.id
            }, function (err, exist_social) {
                if (err) return res.json(resErr(err));
                if (!exist_social) {
                    Social.create({
                            provider: social._provider,
                            id: social._profile.id,
                            name: social._profile.name,
                            firstName: social._profile.firstName,
                            lastName: social._profile.lastName,
                            email: social._profile.email,
                            profilePicURL: social._profile.profilePicURL,
                            userId: user._id
                        }, function (err, new_social) {
                            if (err) return res.json(resErr(err));
                            User.findByIdAndUpdate(user._id,
                                {$addToSet: {social_connect: new_social._id}},
                                {safe: true, upsert: true},
                                function (err, user) {
                                    if (err) return res.json(resErr(err));
                                    const randomPassword = Crypto.randomBytes(4).toString('hex');
                                    user.photo = downloadProfilePhoto(social._profile.profilePicURL);
                                    user.name = social._profile.name;
                                    user.encrypt_password = randomPassword;
                                    user.invitation = null;
                                    user.save(function (err, user) {
                                        if (err) return res.json(resErr(err));
                                        sendMail('password', user.email, {
                                            host: process.env.HOST,
                                            password: randomPassword,
                                            emailFrom: process.env.EMAIL_FROM
                                        }, sendres => {
                                            if (sendres.status) {
                                                res.json(resOk({
                                                    token: user.getToken()
                                                }));
                                            } else {
                                                return res.json(resErr(sendres.error));
                                            }
                                        });
                                    });
                                }
                            );
                        }
                    );
                } else {
                    return res.json(resErr('This social profile already linked with another user'));
                }
            });
        } else {
            return res.json(resErr('User profile not found doesn\'t exist, please check'));
        }
    });
}

function connect(req, res) {
    const {_provider, _profile, _token} = req.body;

    if (!_provider || !_profile || !_token) {
        return res.json(resErr('Unable to receive data from provider!'));
    }

    Social.findOne({
        provider: _provider,
        id: _profile.id
    }, function (err, exist_social) {
        if (err) return res.json(resErr(err));
        if (!exist_social) {
            Social.create({
                provider: _provider,
                id: _profile.id,
                name: _profile.name,
                firstName: _profile.firstName,
                lastName: _profile.lastName,
                email: _profile.email,
                profilePicURL: _profile.profilePicURL,
                userId: req.userId
            }, function (err, social) {
                if (err) return res.json(resErr(err));
                User.findByIdAndUpdate(req.userId,
                    {$addToSet: {social_connect: social._id}},
                    {safe: true, upsert: true},
                    function (err) {
                        if (err) return res.json(resErr(err));
                        res.json(resOk('ok'));
                    }
                );
            });
        } else {
            res.json(resErr('This social profile already linked with another user'));
        }
    });
}

function disconnect(req, res) {
    const {id} = req.params;

    Social.findByIdAndDelete(id, function (err) {
        if (err) return res.json(resErr(err));
        res.json(resOk('ok'));
    });
}

module.exports = {auth, registration, connect, disconnect};
