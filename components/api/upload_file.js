import {resOk, resErr} from "../helpers/response";
import crypto from "crypto";
import path from "path";

const fileExtensions = {
    profile: {'jpg': 1, 'jpeg': 1, 'png': 1},
    session: {'pdf': 1, 'doc': 1, 'docx': 1},
    compensation: {'pdf': 1}
};

function uploadFile(req, res) {
    const {type} = req.params;
    const uploadFile = req.files.file;
    const fileExtension = path.extname(req.files.file.name).toLowerCase();

    if (fileExtensions[type][fileExtension.substr(1)]) {
        const fileName = crypto.randomBytes(16).toString('hex') + fileExtension;
        uploadFile.mv(__dirname + '/../../files/temp/' + fileName, err => {
                if (err) return res.json(resErr(err));
                res.json(resOk({
                    file: fileName
                }));
            }
        );
    } else {
        res.json(resErr('This type of files doesn\'t support please try ' + Object.keys(fileExtensions[type]).join(', ')));
    }
}

module.exports = {uploadFile};
