import {resOk, resErr} from "../helpers/response";
import {protection} from "../helpers/user_protection";
import User from "../../models/user";
import Payment from "../../models/payments";

function createPayment(req, res) {
    const {
        user_id, amount, date
    } = req.body;

    if (!user_id) {
        return res.json(resErr('User ID is required'));
    }

    if (!amount) {
        return res.json(resErr('Amount is required'));
    }

    if (!date) {
        return res.json(resErr('Date is required'));
    }

    User.findById(user_id, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            Payment.create({
                user_id, amount, date
            }, function (err, payment) {
                if (err) return res.json(resErr(err));
                User.findByIdAndUpdate(user_id,
                    {$addToSet: {payments: payment._id}},
                    {safe: true, upsert: true},
                    function (err) {
                        if (err) return res.json(resErr(err));
                        res.json(resOk('ok'));
                    }
                );
            });
        } else {
            res.json(resErr('User profile not found doesn\'t exist, please check'));
        }
    });
}

function getPayment(req, res) {
    const {id} = req.params;

    Payment.findById(id).populate({
        path: 'user_id',
        select: protection
    }).exec(function (err, payment) {
        if (err) return res.json(resErr(err));
        if (payment) {
            res.json(resOk(payment));
        } else {
            res.json(resErr('Payment can not be found or doesn\'t exist, please check'));
        }
    });
}

function updatePayment(req, res) {
    const {id} = req.params;
    const {
        amount, date
    } = req.body;

    if (!amount || amount === 0) {
        return res.json(resErr('Amount is required'));
    }

    if (!date) {
        return res.json(resErr('Date is required'));
    }

    Payment.findByIdAndUpdate(id,
        {amount, date},
        function (err) {
            if (err) return res.json(resErr(err));
            res.json(resOk('ok'));
        }
    );
}

function deletePayment(req, res) {
    const {id} = req.params;

    Payment.findByIdAndDelete(id, function (err) {
            if (err) return res.json(resErr(err));
            res.json(resOk('ok'));
        }
    );
}

module.exports = {createPayment, getPayment, updatePayment, deletePayment};
