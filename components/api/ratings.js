import Rating from "../../models/ratings";
import User from '../../models/user';
import {resOk, resErr} from "../helpers/response";

function getRating(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("User ID is required!"));
    }

    User.findById(id).populate({
        path: 'rating'
    }).exec(function (err, user) {
        if (err) return res.json(resErr(err));
        let ratings_array = user.rating.map(function (v) {
            return v.rating
        });

        let sum = 0;
        for (let i = 0; i < ratings_array.length; i++) {
            sum += ratings_array[i];
        }
        let avg_rating = sum / ratings_array.length;
        res.json(resOk({rating: avg_rating}));
    })

}

function createRating(req, res) {
    const {user_id, rating} = req.body;

    if (!user_id) {
        return res.json(resErr("User ID is required!"));
    }

    if (!rating) {
        return res.json(resErr("Rating value is required!"));
    }

    if (user_id === req.userId) {
        return res.json(resErr("Sorry, but you are not able to rate yourself!"));
    }

    Rating.findOne({user_from: req.userId, user_id}, function (err, exists_rating) {
        if (err) return res.json(resErr(err));
        if (exists_rating && !(req.userRole === 'admin' || req.userRole === 'superadmin')) return res.json(resErr("Rating already has been set for this user!"));
        Rating.create({user_id, rating, user_from: req.userId}, function (err, rating) {
            if (err) return res.json(resErr(err));
            User.findByIdAndUpdate(user_id,
                {$addToSet: {rating: rating._id}},
                {safe: true, upsert: true},
                function (err) {
                    if (err) return res.json(resErr(err));
                    res.json(resOk(rating));
                }
            );
        });
    });
}

module.exports = {getRating, createRating};
