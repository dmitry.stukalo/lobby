import State from "../../models/states";
import User from "../../models/user";
import {resOk, resErr} from "../helpers/response";
import {upload_file} from "../helpers/upload_file";
import {protection} from "../helpers/user_protection";
import EmailValidator from "email-validator";
import Target from "../../models/targets";

function getLobbyists(req, res) {
    User.find({
        role: 'lobbyist',
        invitation: null
    }).select(protection)
        .populate('states rating company_id')
        .exec(function (err, users) {
            if (err) return res.json(resErr(err));
            if (users) {
                res.json(resOk(users));
            } else {
                res.json(resErr("Lobbyists not found"));
            }
        });
}

function createLobbyist(req, res) {
    const {
        role, name, email, photo,
        password, password_confirm
    } = req.body;

    if (!role) {
        return res.json(resErr("Role is required"));
    }

    if (role === 'admin' && req.userRole !== 'superadmin') {
        return res.json(resErr('Sorry, access denied, please check your permissions'));
    }

    if (!name) {
        return res.json(resErr("Name is required"));
    }

    if (!email) {
        return res.json(resErr("Email can not be empty"));
    }

    if (!EmailValidator.validate(email)) {
        return res.json(resErr("Email format is not valid, please check"));
    }

    if (!password) {
        return res.json(resErr("Password is required"));
    }

    if (!password_confirm) {
        return res.json(resErr("Password confirmation can not be empty"));
    }

    if (password !== password_confirm) {
        return res.json(resErr('Passwords do not match, please check'));
    }

    User.findOne().byEmail(email).exec(function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            res.json(resErr('Sorry, this email already exists, please try another email'));
        } else {
            User.create({role, name, email}, function (err, user) {
                if (err) return res.json(resErr(err));
                user.photo = upload_file('profile', photo, '');
                user.encrypt_password = password;
                user.save(function (err) {
                    if (err) return res.json(resErr(err));
                    res.json(resOk({
                        userId: user._id
                    }));
                });
            });
        }
    });
}

function createRelation(req, res) {
    const {user_id, state_id} = req.body;

    if (!user_id) {
        return res.json(resErr("User ID is required"));
    }

    if (!state_id) {
        return res.json(resErr("State is required"));
    }

    User.findById(user_id, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user.role !== 'lobbyist') return res.json(resErr('Lobbyist role should be assigned for this user'));
        User.findByIdAndUpdate(user_id,
            {$addToSet: {states: state_id}},
            {safe: true, upsert: true},
            function (err) {
                if (err) return res.json(resErr(err));
                State.findByIdAndUpdate(state_id, {
                        $addToSet: {lobbyists: user_id}
                    },
                    {
                        safe: true, upsert: true
                    }, function (err) {
                        if (err) return res.json(resErr(err));
                        res.json(resOk('ok'));
                    }
                );
            }
        );
    });
}

function getStatesByUserId(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("User ID is required"));
    }

    User.findById(id).populate('states').exec(function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            res.json(resOk(user.states));
        } else {
            res.json(resErr("User profile not found doesn\'t exist, please check"));
        }
    });
}

function getUsersByStateId(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("State is required"));
    }

    State.findById(id).populate('lobbyists').exec(function (err, state) {
        if (err) return res.json(resErr(err));
        if (state) {
            res.json(resOk(state.lobbyists));
        } else {
            res.json(resErr("State can not be found or doesn\'t exist, please check"));
        }
    });
}

function deleteRelation(req, res) {
    const {user_id, state_id} = req.body;

    if (!user_id) {
        return res.json(resErr("User ID is required!"));
    }

    if (!state_id) {
        return res.json(resErr("State is required!"));
    }

    User.findByIdAndUpdate(user_id,
        {$pull: {states: state_id}},
        {safe: true, upsert: true},
        function (err) {
            if (err) return res.json(resErr(err));
            State.findByIdAndUpdate(state_id,
                {$pull: {lobbyists: user_id}},
                {safe: true, upsert: true},
                function (err) {
                    if (err) return res.json(resErr(err));
                    Target.updateMany({
                            stateId: state_id,
                            lobbyists: {$all: [{_id: user_id}]}
                        },
                        {$pull: {lobbyists: user_id}},
                        {safe: true, upsert: true},
                        function () {
                            res.json(resOk('ok'));
                        }
                    );
                }
            );
        }
    );
}

function deleteLobbyist(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("User ID is required!"));
    }

    User.findById(id, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user.role !== 'lobbyist') return res.json(resErr('Lobbyist role should be assigned for this user'));
        User.findByIdAndDelete(id, function (err) {
                if (err) return res.json(resErr(err));
                res.json(resOk('ok'));
            }
        );
    });
}

module.exports = {
    getLobbyists,
    createLobbyist,
    createRelation,
    getStatesByUserId,
    getUsersByStateId,
    deleteRelation,
    deleteLobbyist
};
