import User from "../../models/user";
import Company from "../../models/companies";
import {resOk, resErr} from "../helpers/response";

function getCompanies(req, res) {
    Company.find().exec(function (err, companies) {
            if (err) return res.json(resErr(err));
            res.json(resOk(companies));
        }
    );
}

function createCompany(req, res) {
    const {name, url} = req.body;
    let data;

    if (!name) {
        return res.json(resErr("Name is required!"));
    }

    if (req.userRole === 'superadmin' || req.userRole === 'admin') {
        data = {name, url}
    } else {
        data = {name, users: req.userId}
    }

    Company.create(data, function (err, company) {
        if (err) return res.json(resErr(err));
        if (req.userRole !== 'superadmin' && req.userRole !== 'admin') {
            User.findByIdAndUpdate(req.userId,
                {company_id: company._id},
                function (err) {
                    if (err) return res.json(resErr(err));
                    res.json(resOk('ok'));
                }
            );
        } else {
            res.json(resOk('ok'));
        }
    });
}

function getCompany(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("Company ID is required!"));
    }

    Company.findById(id, function (err, company) {
        if (err) return res.json(resErr(err));
        if (company) {
            res.json(resOk(company));
        } else {
            return res.json(resErr('Company can not be found or doesn\'t exist, please check'));
        }
    });
}

function updateCompany(req, res) {
    const {id} = req.params;
    const {name, url} = req.body;

    if (!id) {
        return res.json(resErr("Company ID is required!"));
    }

    if (!name) {
        return res.json(resErr("Name is required!"));
    }

    if (!url) {
        return res.json(resErr("URL is required!"));
    }

    Company.findByIdAndUpdate(id, {name, url}, function (err, company) {
        if (err) return res.json(resErr(err));
        if (company) {
            res.json(resOk('ok'));
        } else {
            return res.json(resErr('Company can not be found or doesn\'t exist, please check'));
        }
    });
}

function deleteCompany(req, res) {
    const {id} = req.params;

    Company.findByIdAndDelete(id, function (err, company) {
        if (err) return res.json(resErr(err));
        if (company) {
            res.json(resOk('ok'));
        } else {
            return res.json(resErr('Company can not be found or doesn\'t exist, please check'));
        }
    });
}

module.exports = {getCompanies, getCompany, createCompany, updateCompany, deleteCompany};
