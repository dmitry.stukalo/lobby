import User from "../../models/user";
import {resOk, resErr} from "../helpers/response";
import {protection} from "../helpers/user_protection";

function getAdmins(req, res) {
    User.find({
        role: 'admin',
        invitation: null
    }).select(protection).exec(function (err, users) {
        if (err) return res.json(resErr(err));
        res.json(resOk(users));
    });
}

function deleteAdmin(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("User ID is required!"));
    }

    User.findOneAndDelete({
            _id: id,
            role: 'admin'
        }, function (err, user) {
            if (err) return res.json(resErr(err));
            if (user) {
                res.json(resOk('ok'));
            } else {
                return res.json(resErr('User profile not found doesn\'t exist, please check'));
            }
        }
    );
}

module.exports = {
    getAdmins,
    deleteAdmin
};
