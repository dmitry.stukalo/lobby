import User from "../../models/user";
import Company from "../../models/companies";
import {resErr, resOk} from "../helpers/response";
import {protection} from "../helpers/user_protection";
import {upload_file} from "../helpers/upload_file";
import EmailValidator from "email-validator";
import {sendMail} from "../helpers/send_mail";

function getProfile(req, res) {
    const {id} = req.params;

    User.findById(req.userId, function (err, user) {
        if (err) return res.json(resErr(err));
        User.findById(id)
            .populate('company_id', Company)
            .populate('states')
            .populate({
                path: 'notes',
                options: {
                    sort: {
                        createdAt: -1
                    },
                    limit: 10
                },
                populate: {
                    path: 'user_id state_id',
                    select: protection
                }
            })
            .populate('rating')
            .populate('payments')
            .populate('social_connect')
            .select(protection)
            .exec(function (err, lobbyist) {
                    if (err) return res.json(resErr(err));
                    if (lobbyist) {

                        if (req.userRole === 'lobbyist' && !compareStates(user.states, lobbyist.states.map(v => v._id.toString()))) {
                            return res.json(resErr('Sorry, access denied, please check your permissions'));
                        }

                        res.json(resOk(lobbyist));
                    } else {
                        res.json(resErr('User profile not found doesn\'t exist, please check'));
                    }
                }
            );
    });
}

function compareStates(user_states, lobbyist_states) {
    let states = 0;
    user_states.forEach(user_state => {
        if (lobbyist_states.find(lobbyist_state => user_state.toString() === lobbyist_state.toString())) {
            states++;
        }
    });
    return states !== 0;
}

function updateProfile(req, res) {
    const {id} = req.params;

    const {
        name, email, photo, location,
        address, skype, linkedin, additional_info,
        compensation, compensation_file, stock, stock_date,
        company_name, company_id
    } = req.body;

    if (req.userId !== id && req.userRole !== 'superadmin' && req.userRole !== 'admin') {
        return res.json(resErr('Sorry, access denied, please check your permissions'));
    }

    if (email) {
        if (!EmailValidator.validate(email)) {
            return res.json(resErr("Email format is not valid, please check!"));
        }
    }

    User.findById(id, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            user.name = name;
            if (email && user.email !== email) {
                user.email = email;
            }
            user.location = location;
            user.address = address;
            user.skype = skype;
            user.linkedin = linkedin;
            user.additional_info = additional_info;
            user.photo = upload_file('profile', photo, user.photo);
            user.compensation = compensation;
            user.compensation_file = upload_file('compensation', compensation_file, user.compensation_file);
            user.stock = stock;
            user.stock_date = stock_date;
            if (!company_name) {
                user.company_id = null;
                user.save(function (err) {
                    if (err) return res.json(resErr(err));
                    res.json(resOk('ok'));
                });
            } else {
                if (!company_id) {
                    Company.create({name: company_name}, function (err, company) {
                        if (err) return res.json(resErr(err));
                        user.company_id = company._id;
                        user.save(function (err) {
                            if (err) return res.json(resErr(err));
                            res.json(resOk('ok'));
                        });
                    });
                } else {
                    user.company_id = company_id;
                    user.save(function (err) {
                        if (err) return res.json(resErr(err));
                        res.json(resOk('ok'));
                    });
                }
            }
        } else {
            return res.json(resErr('User profile not found doesn\'t exist, please check'));
        }
    });
}

function changePassword(req, res) {
    const {
        user_id, current_password,
        new_password, new_password_confirm
    } = req.body;

    if (req.userId !== user_id && req.userRole !== 'superadmin' && req.userRole !== 'admin') {
        return res.json(resErr('Sorry, access denied, please check your permissions'));
    }

    if (req.userId === user_id || !(req.userRole === 'superadmin' || req.userRole === 'admin')) {
        if (!current_password) {
            return res.json(resErr("Password can not be empty!"));
        }
    }

    if (!new_password) {
        return res.json(resErr("New password is required!"));
    }

    if (!new_password_confirm) {
        return res.json(resErr("Password confirmation can not be empty!"));
    }

    if (new_password !== new_password_confirm) {
        return res.json(resErr('Passwords do not match, please check'));
    }

    User.findById(user_id, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user) {
            if ((req.userRole === 'superadmin' || req.userRole === 'admin')  && req.userId !== user_id){
                user.encrypt_password = new_password;
                user.save(function (err) {
                    if (err) return res.json(resErr(err));
                    sendMail('password', user.email, {
                        host: process.env.HOST,
                        password: new_password,
                        emailFrom: process.env.EMAIL_FROM
                    });
                    res.json(resOk('ok'));
                });
            } else {
                if (user.authenticate(current_password)) {
                    user.encrypt_password = new_password;
                    user.save(function (err) {
                        if (err) return res.json(resErr(err));
                        sendMail('password', user.email, {
                            host: process.env.HOST,
                            password: new_password,
                            emailFrom: process.env.EMAIL_FROM
                        });
                        res.json(resOk('ok'));
                    });
                } else {
                    return res.json(resErr("You have entered incorrect password!"));
                }
            }
        } else {
            return res.json(resErr("User profile not found doesn\'t exist, please check"));
        }
    });
}

module.exports = {getProfile, updateProfile, changePassword};
