import States from '../../models/states';
import User from '../../models/user';
import StatesDates from '../../models/states_dates';
import Dictionaries from '../../models/dictionaries';
import {resOk, resErr} from "../helpers/response";
import {upload_file} from "../helpers/upload_file";
import {protection} from "../helpers/user_protection";
import {sendMail} from "../helpers/send_mail";

function getStates(req, res) {
    States.find().populate({
        path: 'lobbyists',
        select: protection
    })
        .populate('dates')
        .populate('status')
        .exec(function (err, states) {
            if (err) return res.json(resErr(err));
            res.json(resOk(states));
        });
}

function getState(req, res) {
    const {id} = req.params;

    States.findById(id).populate({
        path: 'lobbyists',
        select: protection
    })
        .populate('dates status overbillings')
        .exec(function (err, state) {
            if (err) return res.json(resErr(err));

            if (req.userRole === 'lobbyist') {
                const access = state.lobbyists.find(lobbyist => lobbyist._id.toString() === req.userId);
                if (!access) return res.json(resErr('Sorry, access denied, please check your permissions'));
            }

            res.json(resOk(state));
        });
}

function getStatesAbout(req, res) {
    const {letters} = req.params;

    States.findOne({letters}).populate({
        path: 'lobbyists',
        select: protection
    }).populate({
        path: 'targets',
        populate: {
            path: 'title lobbyists',
            select: protection
        }
    })
        .populate('dates status overbillings')
        .populate({
            path: 'notes',
            options: {
                sort: {
                    createdAt: -1
                }
            },
            populate: {
                path: 'user_id state_id',
                select: protection
            }
        }).exec(function (err, state) {
        if (err) return res.json(resErr(err));

        if (req.userRole === 'lobbyist') {
            const access = state.lobbyists.find(lobbyist => lobbyist._id.toString() === req.userId);
            if (!access) return res.json(resErr('Sorry, access denied, please check your permissions'));
        }

        res.json(resOk(state));
    });
}

function updateState(req, res) {
    const {id} = req.params;
    const {
        status, priority, description
    } = req.body;

    User.findById(req.userId, function (err, user) {
        if (err) return res.json(resErr(err));
        if (!(req.userRole === 'admin' || req.userRole === 'superadmin') && (user.states.indexOf(id) < 0)) {
            return res.json(resErr('Sorry, access denied, please check your permissions'));
        }
        States.findById(id)
            .populate('lobbyists status')
            .exec(function (err, state) {
                    if (err) return res.json(resErr(err));
                    if (req.userRole === 'lobbyist' && state.status.status_type === 'is_lost') {
                        return res.json(resErr('Sorry, access denied, please check your permissions'));
                    }
                    state.priority = priority;
                    state.description = description;
                    if (state.status._id.toString() !== status) {
                        Dictionaries.findById(status, function (err, dic_status) {
                            if (err) return res.json(resErr(err));
                            state.lobbyists.forEach(lobbyist => {
                                if (lobbyist._id.toString() !== req.userId) {
                                    sendMail('state_status_update', lobbyist.email, {
                                        host: process.env.HOST,
                                        user: lobbyist.name,
                                        author: user.name,
                                        state: state.name,
                                        state_letters: state.letters,
                                        old_status: state.status.name,
                                        new_status: dic_status.name,
                                        emailFrom: process.env.EMAIL_FROM
                                    });
                                }
                            });
                        });
                    }
                    state.status = status;
                    state.save(function (err) {
                        if (err) return res.json(resErr(err));
                        res.json(resOk('ok'));
                    });
                }
            );
    });
}

function updateStateDates(req, res) {
    const {id, year} = req.params;
    const {
        session_ends, lobbyist_hired,
        hb_introduced, hb_introduced_file, sb_introduced,
        sb_introduced_file, passed_house_committee,
        passed_senate_committee, passed_house, passed_senate,
        became_law
    } = req.body;

    User.findById(req.userId, function (err, user) {
        if (err) return res.json(resErr(err));
        if (!(req.userRole === 'admin' || req.userRole === 'superadmin') && (user.states.indexOf(id) < 0)) {
            return res.json(resErr('Sorry, access denied, please check your permissions'));
        }

        States.findById(id)
            .populate('status')
            .exec(function (err, state) {
                if (err) return res.json(resErr(err));
                if (req.userRole === 'lobbyist' && state.status.status_type === 'is_lost') {
                    return res.json(resErr('Sorry, access denied, please check your permissions'));
                }
                StatesDates.findOne({
                    state_id: id,
                    year: year
                }, function (err, statesDates) {
                    if (err) return res.json(resErr(err));
                    if (!statesDates) {
                        statesDates = new StatesDates();
                    }
                    statesDates.state_id = id;
                    statesDates.year = year;
                    statesDates.session_ends = session_ends;
                    statesDates.lobbyist_hired = lobbyist_hired;
                    statesDates.hb_introduced = hb_introduced;
                    statesDates.hb_introduced_file = upload_file('session', hb_introduced_file, statesDates.hb_introduced_file);
                    statesDates.sb_introduced = sb_introduced;
                    statesDates.sb_introduced_file = upload_file('session', sb_introduced_file, statesDates.sb_introduced_file);
                    statesDates.passed_house_committee = passed_house_committee;
                    statesDates.passed_senate_committee = passed_senate_committee;
                    statesDates.passed_house = passed_house;
                    statesDates.passed_senate = passed_senate;
                    statesDates.became_law = became_law;
                    statesDates.save(function (err, statesDates) {
                        if (err) return res.json(resErr(err));
                        States.findByIdAndUpdate(id,
                            {$addToSet: {dates: statesDates._id}},
                            {safe: true, upsert: true},
                            function (err) {
                                if (err) return res.json(resErr(err));
                                res.json(resOk('ok'));
                            });
                    });
                });
            });
    });
}

module.exports = {getStates, getState, updateState, getStatesAbout, updateStateDates};
