import Overbilling from "../../models/overbillings";
import States from "../../models/states";
import {resOk, resErr} from "../helpers/response";

function getOverbillings(req, res) {
    Overbilling.find()
        .populate('state_id')
        .exec(function (err, overbillings) {
            if (err) return res.json(resErr(err));
            res.json(resOk(overbillings));
        }
    );
}

function getOverbilling(req, res) {
    const {id} = req.params;

    Overbilling.findById(id)
        .populate('state_id')
        .exec(function (err, overbilling) {
            if (err) return res.json(resErr(err));
            if (!overbilling) return res.json(resErr('Overbilling item not found'));
            res.json(resOk(overbilling));
        }
    );
}

function createOverbilling(req, res) {
    const {
        state_id, company, amount, year, category, preventable_by_tb,
        gist, url, details
    } = req.body;

    if (!state_id) {
        return res.json(resErr("State ID is required!"));
    }

    Overbilling.create({
        state_id, company, amount,
        year, category,
        preventable_by_tb, gist,
        url, details
    }, function (err, overbilling) {
        if (err) return res.json(resErr(err));
        States.findByIdAndUpdate(state_id,
            {$addToSet: {overbillings: overbilling._id}},
            {safe: true, upsert: true},
            function (err) {
                if (err) return res.json(resErr(err));
            }
        );
        res.json(resOk('ok'));
    })
}

function updateOverbilling(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("Overbilling ID is required!"));
    }

    const {
        company, amount, year, category, preventable_by_tb,
        gist, url, details
    } = req.body;

    Overbilling.findByIdAndUpdate(id,
        {
            company, amount, year, category, preventable_by_tb,
            gist, url, details
        },
        function (err, overbilling) {
            if (err) return res.json(resErr(err));
            if (!overbilling) return res.json(resErr('Overbilling item not found'));
            res.json(resOk('ok'));
        }
    );
}

function deleteOverbilling(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("Overbilling ID is required!"));
    }

    Overbilling.findByIdAndDelete(id, function (err, overbilling) {
            if (!overbilling) return res.json(resErr('Overbilling item not found'));
            if (err) return res.json(resErr(err));
            if (overbilling.state_id._id) {
                States.findByIdAndUpdate(overbilling.state_id._id,
                    {$pull: {overbillings: overbilling._id}},
                    {safe: true, upsert: true},
                    function (err) {
                        if (err) return res.json(resErr(err));
                        res.json(resOk('ok'));
                    }
                );
            } else {
                res.json(resOk('ok'));
            }
        }
    );
}

module.exports = {getOverbillings, getOverbilling, createOverbilling, updateOverbilling, deleteOverbilling};
