import Target from "../../models/targets";
import State from "../../models/states";
import {resOk, resErr} from "../helpers/response";
import User from "../../models/user";

function getTargets(req, res) {
    const {letters} = req.params;

    if (!letters) {
        return res.json(resErr('State is empty'));
    }

    if (letters === 'all') {
        Target.find().exec(function (err, targets) {
            if (err) return res.json(resErr(err));
            res.json(resOk(targets));
        });
    } else {
        State.findOne({letters}).populate({
            path: 'targets',
            populate: {
                path: 'title lobbyists',
                select: 'name'
            }
        }).exec(function (err, state) {
            if (err) return res.json(resErr(err));
            if (state) {
                res.json(resOk(state.targets));
            } else {
                res.json(resErr('State can not be found or doesn\'t exist, please check'));
            }
        });
    }
}

function getTarget(req, res) {
    const {id} = req.params;

    Target.findById(id).populate({
        path: 'title lobbyists',
        select: 'name'
    }).exec(function (err, target) {
        if (err) return res.json(resErr(err));
        if (target) {
            res.json(resOk(target));
        } else {
            res.json(resErr('Official person not found doesn\'t exist, please check'));
        }
    });
}

function createTarget(req, res) {
    const {
        title, name, priority, party,
        status, info, letters
    } = req.body;

    if (!title) {
        return res.json(resErr("Official title is required"));
    }

    State.findOne({letters})
        .populate('lobbyists status')
        .exec(function (err, state) {
            if (err) return res.json(resErr(err));
            if (req.userRole === 'lobbyist' && state.status.status_type === 'is_lost') {
                return res.json(resErr('Sorry, access denied, please check your permissions'));
            }
            if (state) {

                if (req.userRole === 'lobbyist') {
                    const isAssign = state.lobbyists.find(v => v._id.toString() === req.userId);
                    if (!isAssign) return res.json(resErr('Sorry, access denied, please check your permissions'));
                }

                Target.create({
                    title: title,
                    name: name,
                    priority: priority,
                    party: party,
                    status: status,
                    info: info,
                    stateId: state._id
                }, function (err, target) {
                    if (err) return res.json(resErr(err));
                    State.findByIdAndUpdate(state._id,
                        {$addToSet: {targets: target._id}},
                        {safe: true, upsert: true},
                        function (err) {
                            if (err) return res.json(resErr(err));
                            res.json(resOk('ok'));
                        }
                    );
                });
            } else {
                res.json(resErr('State can not be found or doesn\'t exist, please check'));
            }
        });
}

function updateTarget(req, res) {
    const {id} = req.params;
    const {
        title, name, priority, party,
        status, info
    } = req.body;

    if (!title) {
        return res.json(resErr("Official title is required"));
    }
    
    Target.findById(id)
        .populate({
            path: 'stateId',
            populate: {
                path: 'status'
            }
        })
        .exec(function (err, target) {
            if (err) return res.json(resErr(err));
            if (req.userRole === 'lobbyist' && target.stateId.status.status_type === 'is_lost') {
                return res.json(resErr('Sorry, access denied, please check your permissions'));
            }
            if (!target) return res.json(resErr('Official person not found doesn\'t exist, please check'));
            target.title = title;
            target.name = name;
            target.priority = priority;
            target.party = party;
            target.status = status;
            target.info = info;
            target.save(function (err) {
                if (err) return res.json(resErr(err));
                res.json(resOk('ok'));
            });
        });


}

function deleteTarget(req, res) {
    const {id} = req.params;

    Target.findByIdAndDelete(id, function (err, target) {
        if (err) return res.json(resErr(err));
        if (!target) return res.json(resErr('Official person not found doesn\'t exist, please check'));
        res.json(resOk('ok'));
    });
}

function addLobbyist(req, res) {
    const {id} = req.params;
    const {lobbyist_id} = req.body;

    if (!lobbyist_id) {
        return res.json(resErr('Lobbyist ID is required'));
    }

    User.findById(lobbyist_id, function (err, user) {
        if (err) return res.json(resErr(err));
        if (user.role !== 'lobbyist') return res.json(resErr('Lobbyist role should be assigned for this user'));
        Target.findByIdAndUpdate(id, {
                $addToSet: {lobbyists: lobbyist_id}
            },
            {safe: true, upsert: true},
            function (err, target) {
                if (err) return res.json(resErr(err));
                User.findByIdAndUpdate(lobbyist_id, {
                        $addToSet: {states: target.stateId}
                    },
                    {safe: true, upsert: true},
                    function (err) {
                        if (err) return res.json(resErr(err));
                        State.findByIdAndUpdate(target.stateId, {
                                $addToSet: {lobbyists: lobbyist_id}
                            },
                            {safe: true, upsert: true},
                            function (err) {
                                if (err) return res.json(resErr(err));
                                res.json(resOk('ok'));
                            }
                        );
                    }
                );
            }
        );
    });
}

function deleteLobbyist(req, res) {
    const {id} = req.params;
    const {lobbyist_id} = req.body;

    if (!lobbyist_id) {
        return res.json(resErr('Lobbyist ID is required'));
    }

    Target.findByIdAndUpdate(id, {
            $pull: {lobbyists: lobbyist_id}
        }, {safe: true, upsert: true},
        function (err) {
            if (err) return res.json(resErr(err));
            res.json(resOk('ok'));
        }
    );
}

module.exports = {getTargets, getTarget, createTarget, updateTarget, deleteTarget, addLobbyist, deleteLobbyist};
