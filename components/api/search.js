import Users from "../../models/user";
import Companies from "../../models/companies";
import States from "../../models/states";
import Notes from "../../models/notes";
import Overbillings from "../../models/overbillings";
import {resOk, resErr} from "../helpers/response";

function add_result({result, title, url, description, created, updated}) {
    result.push({title, url, description, created, updated});
    return result;
}

function search(req, res) {
    const {type, q} = req.body;
    const text = q.trim();
    let search_result = [];

    if (!text) {
        return res.json(resErr("Please enter at least three letters to start search"));
    }
    if (type === 'profile') {
        Users.search(text, function (err, users) {
            if (err) return res.json(resErr(err));
            users.forEach(v => {
                if ((v.role === 'lobbyist' || v.role === 'reviewer') && !v.invitation) {
                    search_result = add_result({
                        result: search_result,
                        title: 'Profile: ' + v.name,
                        url: '/profile/' + v._id,
                        description: 'Role: ' + v.role + "\n" + 'E-mail: ' + v.email,
                        created: v.createdAt,
                        updated: v.updatedAt
                    });
                }
            });
            res.json(resOk(search_result));
        });
    } else if (type === 'company') {
        Companies.search(text, function (err, companies) {
            if (err) return res.json(resErr(err));
            companies.forEach(v => {
                search_result = add_result({
                    result: search_result,
                    title: 'Company: ' + v.name,
                    url: v.url,
                    description: '',
                    created: v.createdAt,
                    updated: v.updatedAt
                });
            });
            res.json(resOk(search_result));
        });
    } else if (type === 'state') {
        States.search(text, function (err, states) {
            if (err) return res.json(resErr(err));
            states.forEach(v => {
                search_result = add_result({
                    result: search_result,
                    title: 'State: ' + v.name,
                    url: '/state/' + v.letters.toLowerCase(),
                    description: 'Capital: ' + v.capital + (v.description ? "\n" + v.description : ''),
                    created: v.createdAt,
                    updated: v.updatedAt
                });
            });
            res.json(resOk(search_result));
        });
    } else if (type === 'note') {
        Notes.find({$text: {$search: text, $language: "en"}}, {score: {$meta: "textScore"}})
            .sort({score: {$meta: "textScore"}})
            .populate('state_id user_id')
            .exec(function (err, notes) {
                if (err) return res.json(resErr(err));
                notes.forEach(v => {
                    search_result = add_result({
                        result: search_result,
                        title: 'Note from ' + v.user_id.name + ' on state ' + v.state_id.name,
                        url: '/notes/#note-' + v._id,
                        description: v.message,
                        created: v.createdAt,
                        updated: v.updatedAt
                    });
                });
                res.json(resOk(search_result));
            });
    } else if (type === 'overbilling') {
        Overbillings.find({$text: {$search: text, $language: "en"}}, {score: {$meta: "textScore"}})
            .sort({score: {$meta: "textScore"}})
            .populate('state_id')
            .exec(function (err, overbilling) {
                if (err) return res.json(resErr(err));
                overbilling.forEach(v => {
                    const nf = new Intl.NumberFormat('en-US');
                    search_result = add_result({
                        result: search_result,
                        title: `Overbilling in state ${v.state_id.name} "${v.company || ''}" (${v.year || ''})`,
                        url: `/overbilling-case/${v._id}`,
                        description: `Amount: ${nf.format(v.amount || 0)}${v.gist ? "\n\n" + v.gist : (v.details ? "\n\n" + v.details : '')}${v.url ? "\n\n" + v.url : ''}`,
                        created: v.createdAt,
                        updated: v.updatedAt
                    });
                });
                res.json(resOk(search_result));
            });
    } else {
        return res.json(resErr("Type is not valid, please check"));
    }
}

module.exports = {search};
