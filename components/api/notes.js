import Notes from '../../models/notes';
import User from '../../models/user';
import State from '../../models/states';
import {resOk, resErr} from "../helpers/response";
import {protection} from "../helpers/user_protection";
import {sendMail} from "../helpers/send_mail";

function getNotes(req, res) {
    const {parent, state, limit} = req.params;

    let condition = {};
    if (parent === 'root') {
        condition = state === 'all' ? {parent_note_id: null} : {parent_note_id: null, state_id: state};
    } else if (parent === 'all') {
        condition = state === 'all' ? {} : {state_id: state};
    } else {
        condition = state === 'all' ? {parent_note_id: parent} : {parent_note_id: parent, state_id: state};
    }

    Notes.find(condition)
        .populate({
            path: 'user_id priority',
            select: protection,
        })
        .populate({
            path: 'state_id',
            populate: {
                path: 'status'
            }
        })
        .limit(parseInt(limit ? limit : 100))
        .sort({createdAt: -1})
        .exec(function (err, notes) {
                if (err) return res.json(resErr(err));
                if (notes) {
                    res.json(resOk(notes.filter(v => v.user_id !== null)));
                } else {
                    res.json(resOk([]));
                }
            }
        );
}

function getNote(req, res) {
    const {id} = req.params;

    Notes.findById(id)
        .populate({
            path: 'user_id state_id priority status',
            select: protection
        })
        .exec(function (err, note) {
            if (err) return res.json(resErr(err));
            if (note) {
                res.json(resOk(note));
            } else {
                res.json(resOk([]));
            }
        });
}

function getNotesByState(req, res) {
    const {id} = req.params;

    Notes.find({
        state_id: id
    }).populate({
        path: 'user_id state_id priority',
        select: protection,
    }).exec(function (err, note) {
        if (err) return res.json(resErr(err));
        if (note) {
            res.json(resOk(note));
        } else {
            res.json(resOk([]));
        }
    });
}

function createNote(req, res) {
    const {
        parent_note_id, state_id,
        message, color, priority,
    } = req.body;

    if (!state_id) {
        return res.json(resErr("State is required"));
    }

    if (!message) {
        return res.json(resErr("Message is required"));
    }

    if (!color) {
        return res.json(resErr("Color is required"));
    }

    if (!priority) {
        return res.json(resErr("Priority is required"));
    }

    State.findById(state_id)
        .populate('status')
        .exec(function (err, state) {
            if (err) return res.json(resErr(err));
            if (req.userRole === 'lobbyist' && state.status.status_type === 'is_lost') {
                return res.json(resErr('Sorry, access denied, please check your permissions'));
            }
            User.findById(req.userId, function (err, user) {
                if (err) return res.json(resErr(err));
                if (user.states.indexOf(state_id) > -1 || req.userRole === 'admin' || req.userRole === 'superadmin') {
                    Notes.create({
                            user_id: req.userId, state_id,
                            parent_note_id: parent_note_id || null,
                            message, color, priority
                        },
                        function (err, note) {
                            if (err) return res.json(resErr(err));
                            user.notes.addToSet(note._id);
                            user.save(function (err) {
                                if (err) return res.json(resErr(err));
                                if (parent_note_id) {
                                    Notes.findById(parent_note_id)
                                        .populate('user_id state_id')
                                        .exec(function (err, parent_note) {
                                            if (err) return res.json(resErr(err));
                                            parent_note.notes.addToSet(note._id);
                                            parent_note.save(function (err) {
                                                if (err) return res.json(resErr(err));
                                                if (req.userId.toString() !== parent_note.user_id._id.toString()) {
                                                    sendMail(priority === 'important' ? 'important_comment' : 'state_comment', parent_note.user_id.email, {
                                                        host: process.env.HOST,
                                                        user: parent_note.user_id.name,
                                                        author: user.name,
                                                        state: parent_note.state_id.name,
                                                        state_letters: parent_note.state_id.letters.toLowerCase(),
                                                        note_id: note._id.toString(),
                                                        comment: message,
                                                        emailFrom: process.env.EMAIL_FROM
                                                    });
                                                }
                                                res.json(resOk('ok'));
                                            });
                                        });
                                } else {
                                    res.json(resOk('ok'));
                                }
                            });
                        });
                } else {
                    return res.json(resErr('Sorry, access denied, please check your permissions'));
                }
            });
        });
}

function updateNote(req, res) {
    const {id} = req.params;
    const {message, color, priority} = req.body;

    if (!id) {
        return res.json(resErr("Note ID is required"));
    }

    if (!message) {
        return res.json(resErr("Message is required"));
    }

    if (!color) {
        return res.json(resErr("Color is required"));
    }

    if (!priority) {
        return res.json(resErr("Priority is required"));
    }

    const condition = req.userRole === 'admin' || req.userRole === 'superadmin' ? {_id: id} : {
        _id: id,
        user_id: req.userId
    };

    Notes.findOne(condition)
        .populate('priority')
        .exec(function (err, note) {
                if (err) return res.json(resErr(err));
                State.findById(note.state_id)
                    .populate('status')
                    .exec(function (err, state) {
                        if (err) return res.json(resErr(err));
                        if (req.userRole === 'lobbyist' && state.status.status_type === 'is_lost') {
                            return res.json(resErr('Sorry, access denied, please check your permissions'));
                        }
                        if (!note) return res.json(resErr('Note can not be found or doesn\'t exist, please check'));
                        note.message = message;
                        note.color = color;
                        note.priority = priority;
                        note.save(function (err) {
                            if (err) return res.json(resErr(err));
                            res.json(resOk('ok'));
                        });
                    });
            }
        );
}

function deleteNote(req, res) {
    const {id} = req.params;

    if (!id) {
        return res.json(resErr("Note ID is required"));
    }

    Notes.findById(id, function (err, note) {
        if (err) return res.json(resErr(err));
        if (!note) return res.json(resErr('Note can not be found or doesn\'t exist, please check'));
        if (note.notes) {
            note.is_active = false;
            note.save(function (err) {
                if (err) return res.json(resErr(err));
                res.json(resOk('ok'));
            });
        } else {
            note.remove().exec(function (err) {
                if (err) return res.json(resErr(err));
                res.json(resOk('ok'));
            });
        }
    });
}

module.exports = {getNotes, getNote, getNotesByState, createNote, updateNote, deleteNote};
