import express from "express";
import middleware from "../components/middleware";
import {fillMap} from "../components/utility/fill-map";
import {dropIndexes} from "../components/utility/drop-indexes";

const router = express.Router();

router.get('/fill-map', middleware, fillMap);
router.get('/drop-indexes', middleware, dropIndexes);

module.exports = router;
