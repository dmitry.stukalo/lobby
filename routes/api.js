import express from "express";
import middleware from "../components/middleware";
import * as RegAuth from "../components/api/reg_auth";
import * as Social from "../components/api/social";
import * as States from "../components/api/states";
import * as Profile from '../components/api/profile';
import * as Targets from '../components/api/targets';
import {uploadFile} from '../components/api/upload_file';
import * as Lobbyists from '../components/api/lobbyist';
import * as Notes from '../components/api/notes';
import * as Ratings from '../components/api/ratings';
import * as Payments from '../components/api/payments';
import * as Admins from '../components/api/admin';
import * as Dictionaries from '../components/api/dictionary';
import * as Company from '../components/api/company';
import * as Search from '../components/api/search';
import * as Overbilling from '../components/api/overbilling';

const router = express.Router();

// Reg & auth
router.post('/invitation', middleware, RegAuth.invitation);
router.post('/check_invitation', RegAuth.check_invitation);
router.post('/restore-password', RegAuth.restorePassword);
router.post('/change-password', RegAuth.changePassword);
router.post('/registration', RegAuth.registration);
router.post('/authenticate', RegAuth.authenticate);
router.post('/check_token', middleware, RegAuth.check_token);

// Social auth & registration & connect
router.post('/social/auth', Social.auth);
router.post('/social/registration', Social.registration);
router.post('/social/connect', middleware, Social.connect);
router.delete('/social/connect/:id', middleware, Social.disconnect);

// States
router.get('/states', middleware, States.getStates);
router.get('/states-about/:letters', middleware, States.getStatesAbout);
router.get('/states/:id', middleware, States.getState);
router.put('/states/:id', middleware, States.updateState);
router.put('/states-dates/:id/:year', middleware, States.updateStateDates);

// Profile
router.get('/profile/:id', middleware, Profile.getProfile);
router.put('/profile/:id', middleware, Profile.updateProfile);
router.put('/profile', middleware, Profile.changePassword);

// Target
router.get('/targets/:letters', middleware, Targets.getTargets);
router.get('/target/:id', middleware, Targets.getTarget);
router.post('/target', middleware, Targets.createTarget);
router.put('/target/:id', middleware, Targets.updateTarget);
router.delete('/target/:id', middleware, Targets.deleteTarget);
router.put('/target/lobbyist/:id', middleware, Targets.addLobbyist);
router.delete('/target/lobbyist/:id', middleware, Targets.deleteLobbyist);

// Upload file
router.post('/upload-file/:type', middleware, uploadFile);

// Lobbyist
router.get('/lobbyists', middleware, Lobbyists.getLobbyists);
router.post('/lobbyist', middleware, Lobbyists.createLobbyist);
router.post('/lobbyist/relation', middleware, Lobbyists.createRelation);
router.get('/lobbyist/states/:id', middleware, Lobbyists.getStatesByUserId);
router.get('/lobbyist/users/:id', middleware, Lobbyists.getUsersByStateId);
router.delete('/lobbyist/relation', middleware, Lobbyists.deleteRelation);
router.delete('/lobbyist/:id', middleware, Lobbyists.deleteLobbyist);

// Notes
router.get('/notes/:parent/:state/:limit', middleware, Notes.getNotes);
router.get('/note/:id', middleware, Notes.getNote);
router.get('/notes-state/:id', middleware, Notes.getNotesByState);
router.post('/note', middleware, Notes.createNote);
router.put('/note/:id', middleware, Notes.updateNote);
router.delete('/note/:id', middleware, Notes.deleteNote);

// Ratings
router.get('/rating/:id', middleware, Ratings.getRating);
router.post('/rating', middleware, Ratings.createRating);

// Payments
router.post('/payment', middleware, Payments.createPayment);
router.get('/payment/:id', middleware, Payments.getPayment);
router.put('/payment/:id', middleware, Payments.updatePayment);
router.delete('/payment/:id', middleware, Payments.deletePayment);

// Admins
router.get('/admins', middleware, Admins.getAdmins);
router.delete('/admin/:id', middleware, Admins.deleteAdmin);

// Dictionaries
router.get('/dictionaries', middleware, Dictionaries.getDictionaries);
router.post('/dictionary', middleware, Dictionaries.createDictionary);
router.get('/dictionary/:id', middleware, Dictionaries.getDictionary);
router.put('/dictionary/:id', middleware, Dictionaries.updateDictionary);
router.delete('/dictionary/:id', middleware, Dictionaries.deleteDictionary);

// Companies
router.get('/companies', middleware, Company.getCompanies);
router.get('/company/:id', middleware, Company.getCompany);
router.post('/company', middleware, Company.createCompany);
router.put('/company/:id', middleware, Company.updateCompany);
router.delete('/company/:id', middleware, Company.deleteCompany);

// Search
router.post('/search', middleware, Search.search);

// Overbilling
router.get('/overbillings', middleware, Overbilling.getOverbillings);
router.get('/overbilling/:id', middleware, Overbilling.getOverbilling);
router.post('/overbilling', middleware, Overbilling.createOverbilling);
router.put('/overbilling/:id', middleware, Overbilling.updateOverbilling);
router.delete('/overbilling/:id', middleware, Overbilling.deleteOverbilling);

module.exports = router;
